import java.util.Scanner;

public class Menu {
    static Scanner sc = new Scanner(System.in);
    static void MenuUtama() {
        System.out.println("---------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volum");
        System.out.println("---------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung Luas Bidang");
        System.out.println("2. Hitung Volum");
        System.out.println("0. Tutup Aplikasi");
        System.out.println("---------------------------------------");
        System.out.print("Masukkan Pilihan Anda: ");
    }
    static void MenuLuas(){
        System.out.println("\n---------------------------------------");
        System.out.println("Pilih bidang yang akan dihitung luasnya");
        System.out.println("---------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi panjang");
        System.out.println("0. kembali ke Menu sebelumnya");
        System.out.println("---------------------------------------");
        System.out.print("Masukkan Pilihan Anda: ");
    }
    static void MenuVolum() {
        System.out.println("\n----------------------------------------");
        System.out.println("Pilih bangun yang akan dihitung volumnya");
        System.out.println("----------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. kembali ke Menu sebelumnya");
        System.out.println("---------------------------------------");
        System.out.print("Masukkan Pilihan Anda: ");
    }
    static void infoLuas(String bidang){
        System.out.println("\n----------------------------------------");
        System.out.println("Anda memilih menghitung luas " + bidang);
        System.out.println("----------------------------------------");
    }
    static void infoVolum(String bangun){
        System.out.println("\n----------------------------------------");
        System.out.println("Anda memilih menghitung volum " + bangun);
        System.out.println("----------------------------------------");
    }
    static void exit(){
        System.out.println("----------------------------------------");
        System.out.println("Tekan apa saja untuk kembali ke menu utama");
        String back = sc.next();
    }
    static void prosesLuas(String bidang){
        System.out.println("\nProcessing........\n");
        System.out.print("Luas " + bidang+ " adalah ");
    }
    static void prosesVolum(String bangun){
        System.out.println("\nProcessing........\n");
        System.out.print("Volum " + bangun+ " adalah ");
    }
    static void salah(){
        System.out.println("\nPilihan anda salah\n");
    }
}