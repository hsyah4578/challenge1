public class Hitung {
    static float Persegi(float sisi){
        return sisi * sisi;
    }
    static float Lingkaran(float rad){
        return (22*rad*rad/7);
    }
    static float Segitiga(float a, float t){
        return ((a*t)/2);
    }
    static float Persegipanjang(float p, float l){
        return (p*l);
    }
    static float Kubus(float s){
        return (s*s*s);
    }
    static float Balok(float p, float l, float t){
        return (p * l * t);
    }
    static float Tabung(float rad, float t){
        return (22 * rad * rad * t / 7);
    }
}