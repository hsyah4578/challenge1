import java.util.Scanner;

public class KalkulatorApp {
    static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        while (true){
            Menu.MenuUtama();
            String pilihMenu = sc.next();
            switch (pilihMenu){
                case "0" :
                    System.out.println("Aplikasi ditutup");
                    return;
                case "1" :
                    Menu.MenuLuas();
                    String pilihLuas = sc.next();
                    if (pilihLuas.equals("0")){
                        break;
                    } else if(pilihLuas.equals("1")){
                        String bangun = "Persegi";
                        Menu.infoLuas(bangun);
                        System.out.print("Masukkan panjang sisi " + bangun + ": ");
                        float sisi = sc.nextFloat();
                        Menu.prosesLuas(bangun);
                        System.out.println(Hitung.Persegi(sisi));
                        Menu.exit();
                    } else if(pilihLuas.equals("2")){
                        String bangun = "Lingkaran";
                        Menu.infoLuas(bangun);
                        System.out.print("Masukkan panjang jari-jari " + bangun + ": ");
                        float radius = sc.nextFloat();
                        Menu.prosesLuas(bangun);
                        System.out.println(Hitung.Lingkaran(radius));
                        Menu.exit();
                    } else if(pilihLuas.equals("3")){
                        String bangun = "Segi tiga";
                        Menu.infoLuas(bangun);
                        System.out.print("Masukkan panjang alas " + bangun + ": ");
                        float alas = sc.nextFloat();
                        System.out.print("Masukkan tinggi " + bangun + ": ");
                        float tinggi = sc.nextFloat();
                        Menu.prosesLuas(bangun);
                        System.out.println(Hitung.Segitiga(alas,tinggi));
                        Menu.exit();
                    }  else if(pilihLuas.equals("4")){
                        String bangun = "Persegi panjang";
                        Menu.infoLuas(bangun);
                        System.out.print("Masukkan panjang " + bangun + ": ");
                        float panjang = sc.nextFloat();
                        System.out.print("Masukkan lebar " + bangun + ": ");
                        float lebar = sc.nextFloat();
                        Menu.prosesLuas(bangun);
                        System.out.println(Hitung.Persegipanjang(panjang,lebar));
                        Menu.exit();
                    } else {
                        Menu.salah();
                    }
                    break;
                case "2" :
                    Menu.MenuVolum();
                    String pilihVolum = sc.next();
                    if (pilihVolum.equals("0")){
                        break;
                    } else if (pilihVolum.equals("1")){
                        String bangun = "Kubus";
                        Menu.infoVolum(bangun);
                        System.out.print("Masukkan panjang sisi " + bangun + ": ");
                        float sisi = sc.nextFloat();
                        Menu.prosesVolum(bangun);
                        System.out.println(Hitung.Kubus(sisi));
                        Menu.exit();
                    } else if (pilihVolum.equals("2")){
                        String bangun = "Balok";
                        Menu.infoVolum(bangun);
                        System.out.print("Masukkan panjang " + bangun + ": ");
                        float panjang = sc.nextFloat();
                        System.out.print("Masukkan lebar " + bangun + ": ");
                        float lebar = sc.nextFloat();
                        System.out.print("Masukkan tinggi " + bangun + ": ");
                        float tinggi = sc.nextFloat();
                        Menu.prosesVolum(bangun);
                        System.out.println(Hitung.Balok(panjang, lebar, tinggi));
                        Menu.exit();
                    } else if (pilihVolum.equals("3")){
                        String bangun = "Tabung";
                        Menu.infoVolum(bangun);
                        System.out.print("Masukkan panjang jari-jari alas " + bangun + ": ");
                        float radius = sc.nextFloat();
                        System.out.print("Masukkan tinggi " + bangun + ": ");
                        float tinggi = sc.nextFloat();
                        Menu.prosesVolum(bangun);
                        System.out.println(Hitung.Tabung(radius, tinggi));
                        Menu.exit();
                    } else {
                        Menu.salah();
                    }
                    break;
                default :
                    Menu.salah();
                    break;
            }
        }
    }
}